package principal;

import conexaoDAO.ProdutoDAO;
import produto.Produto;
import java.util.ArrayList;


public class Principal {
    
    public static void main(String[] args) {
    //Instanciando    
        ProdutoDAO produtoDao = new ProdutoDAO();
        ArrayList <Produto> produtos = new ArrayList();
        Produto produto = new Produto();
 
//   //@INSERT...
//    //Inserindo 1 Item no Banco
//       produto.setNome("Carne");
//        produto.setValor(5.0);
//        produto.setDescricao("Seca");
//        produto.setQtde(2);
//        produtoDao.save(produto);//Salvando no banco
//    //Inserindo 2 Item no Banco   
//        produto.setNome("Maça");
//        produto.setValor(3.9);
//        produto.setDescricao("Fuji");
//        produto.setQtde(17);
//        produtoDao.save(produto);//Salvando no banco 

    //@Preenchendo o Array
    produtos = produtoDao.mostrarProdutos();
    
    //@UPDATE
    //Atualizando Informações do produto
       produto = produtos.get(0);
       produto.setDescricao("Cravinho");
       produtoDao.atualizarProduto(produto);
  
    //@DELETE
    //Deletando produto
//      produto = produtos.get(1);
//      produtoDao.deleteProduto(produto);
       
    //Listando
        for( int f=0; f < produtos.size(); f++ ){
            produtos.get(f).imprimir();
        }    
        
        
        
        
        
        
        
        
    }
    
}
