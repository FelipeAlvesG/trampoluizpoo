package mapper;
import java.util.ArrayList;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

import produto.Produto;

/**
 * Classe (interface) Modelo que contém implementação dos comandos que serão executados
 * na base de dados.
 * */
public interface ProdutoMapper {
    @Insert("INSERT INTO "
            +"produto(nome,descricao,valor,qtde)"
            +"VALUES(#{nome},#{descricao},#{valor},#{qtde})")
    void insertProduto(Produto produto);
        
    @Delete("DELETE FROM produto WHERE id=#{id}")
    void deleteProduto(Produto produto);
    
    @Update("UPDATE produto SET nome=#{nome}, descricao=#{descricao}," +
            " valor=#{valor}, qtde=#{qtde} where id=#{id}")
    void updateProduto(Produto produto);
        
    @Select("SELECT * FROM produto ")
        ArrayList<Produto> selectProduto();
}
