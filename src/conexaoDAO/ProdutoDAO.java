package conexaoDAO;
import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;

import produto.Produto;
import conexao.Conexao;
import mapper.ProdutoMapper;

/**
 * Classe Modelo para gerenciar as ações referentes ao banco de dados
 * para a entidade produto; faz referencia entre entidade e objeto (Produto).
 * */
public class ProdutoDAO {
	/**
	 * Inserir produto na base dados.
	 * Utilizar a classe ProdutoMapper do pacote mapper para realizar
	 * ação no banco de dados
	 * */
	public void save(Produto produto){
		SqlSession session = Conexao.getSqlSessionFactory().openSession();	
		ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
		mapper.insertProduto(produto);
		session.commit();
		session.close();
	}
        public ArrayList<Produto> mostrarProdutos(){
        ArrayList<Produto> produtos = new ArrayList();
        SqlSession session = Conexao.getSqlSessionFactory().openSession();
        ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
        try {
            produtos = mapper.selectProduto();
            return produtos;
        } finally {
            session.close();
        }  
    }
    
    public void atualizarProduto(Produto produto){
        SqlSession session = Conexao.getSqlSessionFactory().openSession();
        ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
        mapper.updateProduto(produto);
        session.commit();
        session.close();
    }
    
    public void deleteProduto(Produto produto){
        SqlSession session = Conexao.getSqlSessionFactory().openSession();
        ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
        mapper.deleteProduto(produto);
        session.commit();
        session.close();
    }
}